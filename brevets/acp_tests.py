'''
Nose tests for acp_times.py

Using my birthday as the start date
Start time is 12:00 AM
'''

import acp_times
import nose
import arrow

date = arrow.Arrow(1998,7,17) 

def test_open_times():
	'''
	Test a set of simple open times from each brevet_distance.
	'''

	
	
	assert acp_times.open_time(160, 200, arrow.get(date)) == (date.shift(hours=4, minutes=42)).isoformat()
	assert acp_times.open_time(200, 300, arrow.get(date)) == (date.shift(hours=5, minutes=53)).isoformat()
	assert acp_times.open_time(370, 400, arrow.get(date)) == (date.shift(hours=11, minutes=12)).isoformat()
	assert acp_times.open_time(530, 600, arrow.get(date)) == (date.shift(hours=16, minutes=28)).isoformat()
	assert acp_times.open_time(990, 1000, arrow.get(date)) == (date.shift(hours=32, minutes=44)).isoformat()

def test_close_times():
	'''
	Test a set of simple close times from each brevet_distance.
	'''

	

	assert acp_times.close_time(160, 200, arrow.get(date)) == (date.shift(hours=10, minutes=40)).isoformat()
	assert acp_times.close_time(200, 300, arrow.get(date)) == (date.shift(hours=13, minutes=20)).isoformat()
	assert acp_times.close_time(370, 400, arrow.get(date)) == (date.shift(hours=24, minutes=40)).isoformat()
	assert acp_times.close_time(530, 600, arrow.get(date)) == (date.shift(hours=35, minutes=20)).isoformat()
	assert acp_times.close_time(990, 1000, arrow.get(date)) == (date.shift(hours=74,minutes=8)).isoformat()
	

def test_equal_brevet_dist():
	'''
	Test open and close times that equal the brevet distance. 
	'''
	
	
	
	assert acp_times.open_time(600, 600, arrow.get(date)) == (date.shift(hours=18,minutes=48)).isoformat()
	assert acp_times.close_time(600, 600, arrow.get(date)) == (date.shift(hours=40)).isoformat()

	assert acp_times.open_time(400, 400, arrow.get(date)) == (date.shift(hours=12, minutes=8)).isoformat()
	assert acp_times.close_time(400, 400, arrow.get(date)) == (date.shift(hours=26, minutes=40)).isoformat()



def test_out_of_bounds():
	'''
	Test control_dist_km that are greater than the brevet_distance. If they are greater, set the control_dist_km
	equal to the brevet_dist_km and evaluate. 
	'''
	

	assert acp_times.open_time(670, 600, arrow.get(date)) == (date.shift(hours=18, minutes=48)).isoformat()
	assert acp_times.close_time(670, 600, arrow.get(date)) == (date.shift(hours=40)).isoformat()

	assert acp_times.open_time(230, 200, arrow.get(date)) == (date.shift(hours=5, minutes=53)).isoformat()
	assert acp_times.close_time(230, 200, arrow.get(date)) == (date.shift(hours=13, minutes=20)).isoformat()

def test_unique():
	'''
	Test unique boundary cases such as control_dist_km = 0 or control_dist_km = -1
	'''

	assert acp_times.open_time(0, 1000, arrow.get(date)) == (date.shift(hours=0, minutes=0)).isoformat()
	assert acp_times.close_time(0, 1000, arrow.get(date)) == (date.shift(hours=1, minutes=0)).isoformat()

	assert acp_times.open_time(-1, 600, arrow.get(date)) == (date.shift(hours=0, minutes=0)).isoformat()
	assert acp_times.close_time(-1, 600, arrow.get(date)) == (date.shift(hours=0, minutes=0)).isoformat()





	