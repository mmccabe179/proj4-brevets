"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
from __future__ import division
import arrow

from datetime import datetime

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

minimum = [(200,34), (200, 32), (200, 30), (400, 28)]
maximum = [(200, 15), (200, 15), (200, 15), (400, 11.428)]
#opens_time = {range(0,200): 34, range(200,400): 32, range(400,600): 30, range(600,1000): 28}

#closes_time = {range(0,200): 15, range(200,400): 15, range(400,600): 15, range(600,1000): 11.428}




def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    #assert(control_dist_km < brevet_dist_km)

    #print("control_dist {}".format(control_dist_km))

    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

    r = control_dist_km
    i = 0
    time = 0

    while(r >= minimum[i][0]):
      time += minimum[i][0]/minimum[i][1]
      r-=minimum[i][0]
      i+=1
      #speed = minimum[i][1]
      #print("speed is {}".format(speed))
      #print("r is {}".format(r))
      #distance = min(r, minimum[i][0])
      #print("distance is {}".format(distance))
      #print("distance/speed is {}".format(distance/speed))
      #time += distance/speed
    if(r > 0):
      time += r/minimum[i][1]


  
    

    hours = int(time)
    #print("time - hours {}".format(time-hours))
    minutes = round(60*(time-hours))

    print("hours: {}".format(hours))
    print("minutes: {}".format(minutes))
    #print("time is {}".format(time))

    brevet_start_time = arrow.get(brevet_start_time)
    opening_time = brevet_start_time.shift(hours=hours, minutes=minutes)
    #print("time is {}".format(opening_time))
    #print(opening_time)
    return opening_time.isoformat()


    


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    #assert(control_dist_km < brevet_dist_km)

    #print("control_dist {}".format(control_dist_km))


    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

    r = control_dist_km
    i=0
    time = 0

    while(r >= maximum[i][0]):
      time += maximum[i][0]/maximum[i][1]
      r-=maximum[i][0]
      i+=1
    if(r > 0):
      time += r/maximum[i][1]



    hours = int(time)
    print("hours {}".format(hours))
    minutes = round(60*(time-hours))
    print("minutes {}".format(minutes))

    brevet_start_time = arrow.get(brevet_start_time)
    closing_time = brevet_start_time.shift(hours=hours, minutes=minutes)
    #print(closing_time)
    return closing_time.isoformat()
    #brevet_start_time = datetime(brevet_start_time)
    #closing_time = datetime.time(hour=hours, minute=minutes)
    #return(datetime.datetime.combine(brevet_start_time, closing_time))

    #closing_time = float(str(hours)+'.'+ str(minutes))
    #print("{}".format(closing_time))
    #brevet_start_time = float(brevet_start_time)
    #return brevet_start_time + closing_time