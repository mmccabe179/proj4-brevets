Author: Maura McCabe

Proj4-brevets implements an ACP brevet control times based on the Randounneurs USA guidelines (https://rusa.org/pages/acp-brevet-control-times-calculator).

acp_times.py does the calculations for the opening and closing times of each brevet distance based on an input (the control brevet distance)

flask_brevets.py creates the interaction between calc.html and acp_times.py in order to correctly display requested data on the server. 

calc.html using javascript and ajax to update the html page without reloading. The information is displayed through jquery. 

The entire program calculates opening and closing times of brevet distances based on control distances entered, start time, and start date. 


